//general includes
#include <math.h>

//Messages

//other resources

//OpenCV
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "droneModuleROS.h"
#include "communication_definition.h"

//DroneMsgsROS
#include "std_msgs/String.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "control/Controller_MidLevel_controlModes.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/setControlMode.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneManagerStatus.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
#include "control/simpletrajectorywaypoint.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/points3DStamped.h"
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/droneMission.h"
#include "droneMsgsROS/droneTask.h"
#include "droneMsgsROS/obsVector.h"
#include "std_srvs/Empty.h"
#include "std_msgs/Int16.h"

#define FREQ_SUB_MISSION_PLANNER    10.0

class droneSubMissionPlannerROSMoudule : public DroneModule
{
public:
  droneSubMissionPlannerROSMoudule();
  ~droneSubMissionPlannerROSMoudule();

public:
     bool init();
     void close();
     void readParameters();
     bool resetValues();

public:

     bool runStart();

public:
     void open(ros::NodeHandle & nIn, std::string ModuleName);


private:

    //droneMsgsROS paramaters
    droneMsgsROS::dronePositionRefCommandStamped  drone_position_reference;
    droneMsgsROS::droneSpeeds drone_speed_reference;
    droneMsgsROS::droneYawRefCommand droneYawToLookMsg;
    droneMsgsROS::dronePositionRefCommand dronePointToLookMsg;
    droneMsgsROS::dronePose   current_drone_position_reference;
    droneMsgsROS::droneSpeeds current_drone_speed_reference;
    droneMsgsROS::dronePose   last_drone_estimated_GMRwrtGFF_pose;
    droneMsgsROS::droneMissionPlannerCommand mission_planner_command_msg;
    droneMsgsROS::droneStatus last_drone_status_msg;
    droneMsgsROS::droneHLCommandAck droneHLCommAckMsg;
    droneMsgsROS::dronePositionRefCommand mission_point;
    droneMsgsROS::droneYawRefCommand droneYawRefCommandMsg;
    droneMsgsROS::points3DStamped mission_points_msg;
    droneMsgsROS::droneMission droneMission;

    //other variables of the code
    bool haltMissionScheduler;
    std_msgs::String droneSpeechCommandMsg;
    std_msgs::Bool droneMissionStateMsg;
    int32_t mp_command;
    int32_t last_drone_manager_status_msg;
    ros::Time taskInitTime;
    int id_task, droneId;
    double distanceToYaw;
    bool flagDistanceToYaw, breakMission, ObjectSeenOnce, NewMissionReceived;
    std::vector<droneMsgsROS::vector3f> mission_points;
    std_msgs::Bool TargetFound;
    std_msgs::Int16 idDrone;

protected:

     //Publishers
     ros::Publisher mission_planer_pub;
     ros::Publisher drone_position_reference_publisher;
     ros::Publisher drone_speeds_reference_publisher;
     ros::Publisher drone_rel_trajectory_reference_publisher;
     ros::Publisher drone_abs_trajectory_reference_publisher;
     ros::Publisher dronePositionRefCommandPubl;
     ros::Publisher drone_yaw_reference_publisher;
     ros::Publisher drone_yaw_to_look_publisher;
     ros::Publisher drone_point_to_look_publisher;
     ros::Publisher drone_mission_state_pub;
     ros::Publisher drone_speech_command_pub;
     ros::Publisher drone_gmp_target_found_ack_pub;

     //Subscribers
     ros::Subscriber drone_status_subs;
     ros::Subscriber drone_manager_status_subs;
     ros::Subscriber droneHLCommAckSub;
     ros::Subscriber drone_estimated_pose_subs;
     ros::Subscriber drone_position_reference_subscriber;
     ros::Subscriber drone_speed_reference_subscriber;
     ros::Subscriber drone_gmp_mission_sub;
     ros::Subscriber drone_gmp_break_mission_sub;
     ros::Subscriber drone_gmp_yaw_ref_sub;
     ros::Subscriber drone_yaw_ref_command_sub;
     ros::Subscriber drone_obs_vector_sub;


     void managerAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg);
     void droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg);
     void droneCurrentSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);
     void drone_estimated_GMR_pose_callback(const  droneMsgsROS::dronePose &msg);
     void droneCurrentManagerStatusSubCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg);
     void droneGMPMissionCallback(const droneMsgsROS::droneMission &msg);
     void droneGMPBreakMissionCallback(const std_msgs::Bool &msg);
     void droneGMPYawToLookCommandCallback(const droneMsgsROS::droneYawRefCommand &msg);
     void droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg);
     void droneObsVectorCallback(const droneMsgsROS::obsVector &msg);

     bool calculatePointDistance();
     bool monitortakingoff(), calculateTaskTimeDuration(), monitorflip();
     bool sendNewTask(), readNewTask(), sendMissionMonitor();
     void sendCommandValues(), sendTaskReady();

};
