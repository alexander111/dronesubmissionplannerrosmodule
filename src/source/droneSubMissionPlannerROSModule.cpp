#include "droneSubMissionPlannerROSModule.h"
using namespace std;

droneSubMissionPlannerROSMoudule::droneSubMissionPlannerROSMoudule() :
DroneModule(droneModule::active, 15.0)
{
  if(!init())
    cout<< "Error in Init" << endl;

   droneId = DroneModule::idDrone;
   cout << "DroneID" << droneId << endl;

  return;
}

droneSubMissionPlannerROSMoudule::~droneSubMissionPlannerROSMoudule()
{
    close();
    return;
}


bool droneSubMissionPlannerROSMoudule::init()
{
   droneHLCommAckMsg.ack = false;
   NewMissionReceived = false;
   breakMission = false;
   ObjectSeenOnce = false;
   id_task = 0;

   return true;
}

void droneSubMissionPlannerROSMoudule::close()
{
   DroneModule::close();
   return;
}

bool droneSubMissionPlannerROSMoudule::resetValues()
{
  //TODO: Reset values if there are any
  if(!DroneModule::resetValues())
     return false;

  return true;
}

bool droneSubMissionPlannerROSMoudule::runStart()
{
  if(!DroneModule::run())
     return false;
   else
      droneMissionStateMsg.data = true;
      drone_mission_state_pub.publish(droneMissionStateMsg);

    return true;
}

void droneSubMissionPlannerROSMoudule::open(ros::NodeHandle &nIn, std::string ModuleName)
{

    DroneModule::open(nIn,ModuleName);

    //Pubishers
    mission_planer_pub                        = n.advertise<droneMsgsROS::droneMissionPlannerCommand>("droneMissionPlannerCommand", 1, true);
    drone_position_reference_publisher        = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1);
    drone_speeds_reference_publisher          = n.advertise<droneMsgsROS::droneSpeeds>("droneSpeedsRefs", 1);
    drone_rel_trajectory_reference_publisher  = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>("droneTrajectoryRefCommand", 1);
    drone_abs_trajectory_reference_publisher  = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>("droneTrajectoryAbsRefCommand", 1);
    drone_yaw_to_look_publisher               = n.advertise<droneMsgsROS::droneYawRefCommand>("droneYawToLook", 1);
    dronePositionRefCommandPubl               = n.advertise<droneMsgsROS::dronePositionRefCommand>("droneMissionPoint", 1, true);
    drone_mission_state_pub                   = n.advertise<std_msgs::Bool>("missionState",1, true);
    drone_speech_command_pub                  = n.advertise<std_msgs::String>("message_to_say",1,true);
    drone_gmp_target_found_ack_pub            = n.advertise<std_msgs::Int16>("globalMissionPlannerTargetFoundAck",1,true);
    drone_point_to_look_publisher             = n.advertise<droneMsgsROS::dronePositionRefCommand>("dronePointToLook",1);

    //Subscribers
    droneHLCommAckSub                         = n.subscribe("droneMissionHLCommandAck", 1, &droneSubMissionPlannerROSMoudule::managerAckCallback,this);
    drone_estimated_pose_subs                 = n.subscribe("EstimatedPose_droneGMR_wrt_GFF", 1, &droneSubMissionPlannerROSMoudule::drone_estimated_GMR_pose_callback,this);
    drone_position_reference_subscriber       = n.subscribe("trajectoryControllerPositionReferencesRebroadcast", 1, &droneSubMissionPlannerROSMoudule::droneCurrentPositionRefsSubCallback, this);
    drone_speed_reference_subscriber          = n.subscribe("trajectoryControllerSpeedReferencesRebroadcast", 1, &droneSubMissionPlannerROSMoudule::droneCurrentSpeedsRefsSubCallback, this);
    drone_manager_status_subs                 = n.subscribe("droneManagerStatus", 1, &droneSubMissionPlannerROSMoudule::droneCurrentManagerStatusSubCallback,this);
    drone_gmp_mission_sub                     = n.subscribe("globalMissionPlannerMission",1,&droneSubMissionPlannerROSMoudule::droneGMPMissionCallback, this);
    drone_gmp_break_mission_sub               = n.subscribe("globalMissionPlannerBreakMissionAck",1,&droneSubMissionPlannerROSMoudule::droneGMPBreakMissionCallback, this);
    drone_yaw_ref_command_sub                 = n.subscribe("droneControllerYawRefCommand",1,&droneSubMissionPlannerROSMoudule::droneYawRefCommandCallback, this);
    drone_obs_vector_sub                      = n.subscribe("arucoObservation",1,&droneSubMissionPlannerROSMoudule::droneObsVectorCallback, this);

    droneModuleOpened = true;

    droneMissionStateMsg.data = false;
    drone_mission_state_pub.publish(droneMissionStateMsg);

    /// WAIT FOR START SERVICE ///
    while(!runStart())
    {
        ros::spinOnce();
    }

    //Excecute the first task given by the global mission planner//
    cout << "Executing the mission" << endl;
    sendNewTask();

    return;

}

//---------------------------------------------------------------------------------------------------------------

//Calbacks for the Subscribers  ------------------------------------------------------------------------------------

void droneSubMissionPlannerROSMoudule::managerAckCallback(const droneMsgsROS::droneHLCommandAck::ConstPtr& msg)
{
  if(msg->ack == true || droneMission.mission[id_task].mpCommand == droneMsgsROS::droneTask::SLEEP)
  {
      cout << "recibo ack" << endl;

      sendCommandValues();
      sendTaskReady();


      ros::spinOnce();
      ros::Duration(1.0).sleep();
      ros::spinOnce();


      while(!sendMissionMonitor() && !breakMission && ros::ok())
      {

         //ROS_INFO("Processing Mission!");
         ros::spinOnce();
         ros::Duration(0.05).sleep();

      }
      if (!breakMission)
      {
        if(id_task < droneMission.mission.size()-1)
          {
          id_task++;        //incrementing the task id to start a new task
          sendNewTask();
          NewMissionReceived = false;
          }
      }
      else
      {
        if(NewMissionReceived)
        {
         breakMission = false;
         id_task = 0;
        }
        sendNewTask();
      }

      }
      else
      {

      ROS_INFO("Oooops, False ack");

      }

  return;
}

void droneSubMissionPlannerROSMoudule::droneCurrentPositionRefsSubCallback(const droneMsgsROS::dronePose::ConstPtr &msg)
{
    current_drone_position_reference = (*msg);
    return;
}

void droneSubMissionPlannerROSMoudule::droneCurrentSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg)
{
    current_drone_speed_reference = (*msg);
    return;
}

void droneSubMissionPlannerROSMoudule::drone_estimated_GMR_pose_callback(const  droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose  = (msg);
    return;
}

void droneSubMissionPlannerROSMoudule::droneObsVectorCallback(const droneMsgsROS::obsVector &msg)
{
  //Take the reading from the aruco observations and execute task

  for(int i=0; i<msg.obs.size(); i++)
    {
      if((msg.obs[i].id == 24) && !ObjectSeenOnce)
    //if((msg.obs[i].id == 27 || msg.obs[i].id == 31 || msg.obs[i].id == 67 || msg.obs[i].id == 25) && !ObjectSeenOnce)
      {
      idDrone.data = droneId;
      drone_gmp_target_found_ack_pub.publish(idDrone);
      ObjectSeenOnce = true;
      }
    }
}

void droneSubMissionPlannerROSMoudule::droneGMPBreakMissionCallback(const std_msgs::Bool &msg)
{
  breakMission = msg.data;
  return;
}

void droneSubMissionPlannerROSMoudule::droneCurrentManagerStatusSubCallback(const droneMsgsROS::droneManagerStatus::ConstPtr &msg)
{
    last_drone_manager_status_msg = msg->status;

    switch(last_drone_manager_status_msg)
    {
        case droneMsgsROS::droneManagerStatus::HOVERING:
            cout<<"Manager Status: HOVERING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::HOVERING_VISUAL_SERVOING:
            cout<<"Manager Status: HOVERING VISUAL SERVOING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDING:
            cout<<"Manager Status: LANDING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::LANDED:
            cout<<"Manager Status: LANDED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::TAKINGOFF:
            cout<<"Manager Status: TAKINGOFF"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_ALTITUD:
            haltMissionScheduler = true;
            cout<<"Manager Status: MOVING MANUAL ALTITUD"<<endl;
            cout<<"halting the mission" << endl;
            while(haltMissionScheduler == true)
            {
                ros::Duration(1).sleep();
                ros::spinOnce();
            }
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_POSITION:
            cout<<"Manager Status: MOVING POSITION"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_SPEED:
            cout<<"Manager Status: MOVING SPEED"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_TRAJECTORY:
            cout<<"Manager Status: MOVING TRAJECTORY"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_VISUAL_SERVOING:
            cout<<"Manager Status: MOVING VISUAL SERVOING"<<endl;
            break;

        case droneMsgsROS::droneManagerStatus::MOVING_MANUAL_THRUST:
            cout<< "Manager Status: MOVING VISUAL SERVOING"<<endl;
            cout<< "halting the Mission"<< endl;
            while(haltMissionScheduler == true)
            {
                ros::Duration(1).sleep();
                ros::spinOnce();
            }
            break;

        case droneMsgsROS::droneManagerStatus::EMERGENCY:
            cout<<"Managet Status: EMERGENCY"<<endl;
            breakMission = true;
            cout<<"Stopping Mission"<<endl;
    }
}


void droneSubMissionPlannerROSMoudule::droneGMPMissionCallback(const droneMsgsROS::droneMission &msg)
{

    NewMissionReceived = true;
    cout << "I received the mission" << endl;
    droneMission = msg;

    droneMsgsROS::droneTask task;

    for(int i=0; i<droneMission.mission.size(); i++)
      {
        task = droneMission.mission[i];
        cout<<"Task ["<<i<<"]"<<endl;
        cout<<"task time: "<<task.time<<endl;
        cout<<"mpCommand: "<<task.mpCommand<<endl;
        cout<<"speech_name: "<<task.speech_name<<endl;
        cout<<"module_names: "<<endl;
        for(int j=0;j<task.module_names.size();j++)
          cout<<task.module_names[j]<<endl;
        cout<<"point:"<<"x: "<<task.point.x<<" ; "<<"y:"<<task.point.y<<" ; "<<"z: "<<task.point.z<<endl;
        cout<<"***"<<endl;
      }

    return;
}

bool droneSubMissionPlannerROSMoudule::sendNewTask()
{
    cout << "I am starting a new task" << endl;

    mission_planner_command_msg.time = ros::Time::now();

    // Command Message //
    switch (droneMission.mission[id_task].mpCommand)
    {
    case droneMsgsROS::droneTask::HOVER:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::HOVER;
        break;
    case droneMsgsROS::droneTask::LAND:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::LAND;
        break;
    case droneMsgsROS::droneTask::TAKE_OFF:
        last_drone_manager_status_msg = droneMsgsROS::droneManagerStatus::TAKINGOFF;
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
        break;
    case droneMsgsROS::droneTask::SLEEP:
        //cmdSent = "SLEEP";
        break;
    case droneMsgsROS::droneTask::MOVE_TRAJECTORY:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
        break;
    case droneMsgsROS::droneTask::EMERGENCY:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_EMERGENCY;
        break;
    case droneMsgsROS::droneTask::MOVE_SPEED:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_SPEED;
        break;
    case droneMsgsROS::droneTask::MOVE_POSITION:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_RIGHT:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_LEFT:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_FRONT:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT;
        break;
    case droneMsgsROS::droneTask::MOVE_FLIP_BACK:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK;
        break;
    default:
        mission_planner_command_msg.mpCommand=droneMsgsROS::droneMissionPlannerCommand::UNKNOWN;
        break;
    }

     std::vector<std::string> modules_names;
     modules_names = droneMission.mission[id_task].module_names;
     mission_planner_command_msg.drone_modules_names = modules_names;

     mission_planer_pub.publish(mission_planner_command_msg);
}





void droneSubMissionPlannerROSMoudule::droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr &msg)
{
    droneYawRefCommandMsg.header=msg->header;
    droneYawRefCommandMsg.yaw=msg->yaw;

    flagDistanceToYaw=true;

    return;
}

//--------------------------------------------------------------------------------------------------------------------------------------------------

bool droneSubMissionPlannerROSMoudule::calculatePointDistance()
{

    /// Position distances! ///
    //Now is a square in x-y. it should be a circle!!
    std::vector<double> distanceToMissionPoint;
    //x
    distanceToMissionPoint.push_back(fabs(last_drone_estimated_GMRwrtGFF_pose.x-mission_point.x));
    //y
    distanceToMissionPoint.push_back(fabs(last_drone_estimated_GMRwrtGFF_pose.y-mission_point.y));
    //z
    distanceToMissionPoint.push_back(fabs(last_drone_estimated_GMRwrtGFF_pose.z-mission_point.z));


    if(flagDistanceToYaw)
    {

        // atan2: in the interval [-pi,+pi] radians
        distanceToYaw=atan2(sin(last_drone_estimated_GMRwrtGFF_pose.yaw)*cos(droneYawRefCommandMsg.yaw)-sin(droneYawRefCommandMsg.yaw)*cos(last_drone_estimated_GMRwrtGFF_pose.yaw),
                            sin(last_drone_estimated_GMRwrtGFF_pose.yaw)*sin(droneYawRefCommandMsg.yaw)+cos(last_drone_estimated_GMRwrtGFF_pose.yaw)*cos(droneYawRefCommandMsg.yaw));

    }

    //Check distances
    bool flagPositionAchieved=false;

    if(distanceToMissionPoint[0] <= 0.2  && distanceToMissionPoint[1] <= 0.2 && distanceToMissionPoint[2] <= 0.2)
    {
        flagPositionAchieved=true;
    }

    //Check yaw distances
    bool flagYawAchieved=false;


    if(flagDistanceToYaw && (distanceToYaw <= 15.0) )
    {
        flagYawAchieved=true;
    }

    //Finish task
    if(flagPositionAchieved && flagYawAchieved)
    {
        return true;
    }

    return false;

}

bool droneSubMissionPlannerROSMoudule::monitortakingoff()
{

    if (last_drone_manager_status_msg!=droneMsgsROS::droneManagerStatus::TAKINGOFF)
    {
      return true;
    }
    else
    {
      return false;
    }
}

bool droneSubMissionPlannerROSMoudule::calculateTaskTimeDuration()
{

     ros::Duration taskDuration;
     taskDuration=ros::Time::now()-taskInitTime;

     if(taskDuration.toSec()>=droneMission.mission[id_task].time)
     {
         return true;
     }

     return false;
}

bool droneSubMissionPlannerROSMoudule::monitorflip()
{
    if(last_drone_manager_status_msg == droneMsgsROS::droneManagerStatus::HOVERING)
      return true;

    return false;
}

void droneSubMissionPlannerROSMoudule::sendCommandValues()
{
  //Publishing Speech message
    if(droneMission.mission[id_task].speech_name.size() != 0)
    {
      droneSpeechCommandMsg.data = "Proxima tarea" + droneMission.mission[id_task].speech_name;
      drone_speech_command_pub.publish(droneSpeechCommandMsg);
    }

  //Publishing Points for Trajectory
    if(droneMission.mission[id_task].point.x == 0 && droneMission.mission[id_task].point.y == 0 && droneMission.mission[id_task].point.z == 0 )
    {
      cout << "No trajectory sent" << endl;
    }
    else
    {
        if(droneMission.mission[id_task].point.x == 0 && droneMission.mission[id_task].point.y == 0)
          {
            cout<<"Move In Altitud"<<endl;
            mission_point.x = last_drone_estimated_GMRwrtGFF_pose.x;
            mission_point.y = last_drone_estimated_GMRwrtGFF_pose.y;
            mission_point.z = droneMission.mission[id_task].point.z;
            dronePositionRefCommandPubl.publish(mission_point);

          }
        else
          {
            mission_point.x = droneMission.mission[id_task].point.x;
            mission_point.y = droneMission.mission[id_task].point.y;
            mission_point.z = droneMission.mission[id_task].point.z;
            dronePositionRefCommandPubl.publish(mission_point);
          }

    }


    // the yaw to look command as well//
    switch(droneMission.mission[id_task].yawSelector)
      {
        case 0:
          break;
        case 1:
          droneYawToLookMsg.yaw = droneMission.mission[id_task].yaw;
          drone_yaw_to_look_publisher.publish(droneYawToLookMsg);
          break;
        case 2:
          dronePointToLookMsg.x = droneMission.mission[id_task].pointToLook.x;
          dronePointToLookMsg.y = droneMission.mission[id_task].pointToLook.y;
          drone_point_to_look_publisher.publish(dronePointToLookMsg);
          break;
      }


    return;
}

void droneSubMissionPlannerROSMoudule::sendTaskReady()
{
   taskInitTime = ros::Time::now();
   return;
}

bool droneSubMissionPlannerROSMoudule::sendMissionMonitor()
{
    switch (droneMission.mission[id_task].mpCommand)
      {
      case droneMsgsROS::droneTask::HOVER:
          if(calculateTaskTimeDuration())
            return true;
          break;
      case droneMsgsROS::droneTask::LAND:
          return true;
          break;
      case droneMsgsROS::droneTask::TAKE_OFF:
          if(monitortakingoff())
            return true;
          break;
      case droneMsgsROS::droneTask::SLEEP:
          if(calculateTaskTimeDuration())
            return true;
          break;
      case droneMsgsROS::droneTask::MOVE_TRAJECTORY:
          if(calculatePointDistance())
            return true;
          break;
      case droneMsgsROS::droneTask::EMERGENCY:
          cout << "In Emergency" << endl;
          break;
      case droneMsgsROS::droneTask::MOVE_SPEED:
          cout << "Moving Speed in not being Monitored" << endl;
          break;
      case droneMsgsROS::droneTask::MOVE_POSITION:
        if(calculatePointDistance())
          return true;;
          break;
      case droneMsgsROS::droneTask::MOVE_FLIP_RIGHT:
          if(monitorflip())
            return true;
          break;
      case droneMsgsROS::droneTask::MOVE_FLIP_LEFT:
          if(monitorflip())
          break;
      case droneMsgsROS::droneTask::MOVE_FLIP_FRONT:
          if(monitorflip())
          break;
      case droneMsgsROS::droneTask::MOVE_FLIP_BACK:
          if(monitorflip())
          break;

      }

}

