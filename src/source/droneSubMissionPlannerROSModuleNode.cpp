#include "ros/ros.h"
#include "droneSubMissionPlannerROSModule.h"
#include "nodes_definition.h"


int main (int argc,char **argv)
{
    //ROS init
    ros::init(argc, argv, "droneSubMissionPlannerROSModule");
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << "droneSubMissionPlannerROSModule" << std::endl;

    droneSubMissionPlannerROSMoudule MydroneSubMissionPlannerROSMoudule;
    MydroneSubMissionPlannerROSMoudule.open(n, "droneSubMissionPlannerROSModule");


        ros::spin();
                   
        return 1;

}
